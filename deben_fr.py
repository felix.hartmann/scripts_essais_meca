#!/usr/bin/env python
# coding: utf-8
# Félix Hartmann (felix.hartmann@inrae.fr) — Avril 2019

"""
Ce script sert à caractériser le comportement mécanique global (MOE,
limite d'élasticité et contrainte maximale) d'éprouvettes à partir de
données acquises lors d'essais de flexion 3 points avec la Deben.
"""

import os
import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
from matplotlib.widgets import SpanSelector
from PyQt5.QtWidgets import QApplication, QFileDialog, QInputDialog, QMessageBox

app = QApplication([''])  # nécessaire pour utiliser les widgets PyQt

# Coefficient de corrélation à utiliser pour la linéarisation :
# coef_lim = 0.995
# nouveau_coef_lim, ok = QInputDialog.getDouble(
#     None, "Coefficient de corrélation", 
#     f"""Le coefficient de corrélation minimal pour la linéarisation est de {coef_lim}.\n
# Vous pouvez le modifier manuellement :""",
#     value=coef_lim, decimals=4)
# if ok:
#     coef_lim = nouveau_coef_lim

# Longueur de la travée (espacement entre les deux appuis)
# Cette valeur est fixe.
L = 30

forme = "Circulaire"

continuer = True    # on parcourt les fichiers d'une série d'expériences

while continuer:
    # Choix du fichier de données et du nom de l'échantillon :

    chemin_fichier, _ = QFileDialog.getOpenFileName(
        parent=None, caption="Sélection du fichier issu de la Deben", filter="*.MTR")

    dossier, nom_fichier = os.path.split(chemin_fichier)
    os.chdir(dossier)

    nom_échantillon = QInputDialog.getText(None, "Nom de l'échantillon",
                                           "Nom de l'échantillon :")[0].replace(' ', '_')

    data = pd.read_csv(
            nom_fichier, sep=",", decimal='.', encoding='utf-8', header=None, skiprows=20,
            usecols=[1, 2], names=["Déplacement", "Force"])

    # Géométrie de l'échantillon :

    formes = ["Circulaire", "Ovale", "Rectangulaire", "Triangulaire"]
    forme, _ = QInputDialog.getItem(
            None, "Forme de l'échantillon",
            "Forme de l'échantillon :", formes, formes.index(forme), False)

    if forme=="Circulaire":
        e, _ = QInputDialog.getDouble(None, "Diamètre de l'échantillon",
                                      "Diamètre de l'échantillon (mm) :", decimals=2)
        # On calcule le moment quadratique de l'échantillon :
        I = (np.pi * e**4 * 10**-12) / 64
    elif forme=="Ovale":
        e, _ = QInputDialog.getDouble(
            None, "Diamètre // de l'échantillon",
            "Diamètre de l'échantillon dans la direction parallèle à la flexion (mm) :",
            decimals=2)
        lg, _ = QInputDialog.getDouble(
            None, "Diamètre ⊥ de l'échantillon",
            "Diamètre de l'échantillon dans la direction perpendiculaire à la flexion (mm) :",
            decimals=2)
        # On calcule le moment quadratique de l'échantillon :
        I = (np.pi * lg * e**3 * 10**-12) / 64
    elif forme=="Rectangulaire":
        # L'épaisseur de l'échantillon correspond à sa dimension dans la direction de la flexion.
        e, _ = QInputDialog.getDouble(
            None, "Épaisseur de l'échantillon",
            "Épaisseur de l'échantillon dans la direction de la flexion (mm) :",
            decimals=2)
        lg, _ = QInputDialog.getDouble(None, "Largeur de l'échantillon",
                                       "Largeur de l'échantillon (mm) :", decimals=2)
        # On calcule le moment quadratique de l'échantillon :
        I = (lg * (e**3) * (10**(-12))) / 12
    elif forme=="Triangulaire":
        e, _ = QInputDialog.getDouble(
                None, "Hauteur du triangle",
                "Hauteur de l'échantillon parallèle à la flexion (mm) :", decimals=2)
        b, _ = QInputDialog.getDouble(
                None, "Base du triangle",
                "Base de l'échantillon perpendiculaire à la flexion (mm) :", decimals=2)
        # On calcule le moment quadratique de l'échantillon :
        I = (b * (e**3) * (10**(-12))) / 36

    # Il est possible d'entrer la valeur exacte du moment quadratique.
    # Utile pour des formes irrégulières dont I est estimé par analyse d'image.
#     I_exact, ok = QInputDialog.getDouble(
#         None, "Moment quadratique de l'échantillon",
#         f"""Le moment quadratique calculé est de {I:.2e}m⁴.\n
#     Si vous avez la valeur exacte du moment quadratique de la section à mi-travée, entrez-la manuellement :""",
#         value=I, decimals=15)
#     if ok:
#         I = I_exact

    # Tracé de la courbe Force-Déplacement
    figForceDeplacement, axForceDeplacement = plt.subplots()
    data.plot("Déplacement", "Force", ax=axForceDeplacement, legend=False, grid=True)
    axForceDeplacement.set_title("Courbe Force-Déplacement")
    axForceDeplacement.set_xlabel("Déplacement (mm)")
    axForceDeplacement.set_ylabel("Force (N)")
    figForceDeplacement.savefig(nom_échantillon + "_courbe_force_deplacement.pdf",
                                bbox_inches='tight')
    plt.close(figForceDeplacement)

    """
    # Passage en contraintes et déformations
    # Méthode de Benjamin pour avoir les contraintes, y compris la contrainte max.
    # Ici, on se contente de la courbe force-déplacement, avec la relation
    # d = F * L**3 / (48 * EI)

    # On transforme les déplacements en déformations (cas de la flexion 3 points)
    # avec formule établie à partir de celle de la flèche et de celle de sigma.
    data["Déformation"] = 6 * e * data.Déplacement / L**2

    # On transforme les forces en contraintes maximales (en L/2).
    # Moment fléchissant à mi-travée :
    data["Moment"] = (data.Force * L * 10**-3) / 4
    # Contrainte à mi-travée (MPa) :
    data["Contrainte"] = (data.Moment * (e / 2) / I) * 10**-9  # MPa
    """

    # Choix semi-manuel des limites du domaine élastique :

    bornes = {"inf": data.Déplacement.min(), "sup": data.Déplacement.max()}

    def onselect(d_min, d_max):
        """Appelée lorsque la plage de déplacemnents a été choisie."""
        if d_min < data.Déplacement.min():
            d_min = data.Déplacement.min() 
        if d_max > data.Déplacement.max():
            d_max = data.Déplacement.max() 
        bornes["inf"], bornes["sup"] = d_min, d_max
        réponse = QMessageBox.question(None, "Choix des bornes du domaine élastique",
                                       f"""Vous avez choisi les bornes suivantes :\n
    Borne inférieure : {d_min:.2e} mm
    Borne supérieure : {d_max:.2e} mm\n
Ce choix vous convient-il ?""", defaultButton=QMessageBox.Yes)
#     La borne supérieure sera affinée automatiquement si besoin pour que le coefficient de corrélation minimal soit atteint.\n
        if réponse == QMessageBox.Yes:
            plt.close(figForceDéplacement)

    # Tracé de la courbe Force-Déformation pour le choix manuel du domaine élastique
    figForceDéplacement, axForceDéplacement = plt.subplots()
    data.plot("Déplacement", "Force", ax=axForceDéplacement, legend=False, grid=True)
    axForceDéplacement.set_title("Veuillez sélectionner le domaine élastique")
    axForceDéplacement.set_xlabel("Déplacement (mm)")
    axForceDéplacement.set_ylabel("Force (N)")
    # Widget Matplotlib pour choisir graphiquement la plage :
    span = SpanSelector(axForceDéplacement, onselect, 'horizontal', useblit=True,
                        rectprops=dict(alpha=0.5, facecolor='red'), span_stays=True)
    plt.show()

    # Initialisation de la plage d'ajustement :
    début_plage = (data.Déplacement >= bornes["inf"]).idxmax()
    fin_plage = (data.Déplacement >= bornes["sup"]).idxmax()
    plage = slice(début_plage, fin_plage)

    Déplacement_plage = data.Déplacement[plage]
    # Ajout d'une colonne constante pour obtenir un intercept
    Déplacement_matrice = sm.add_constant(Déplacement_plage)
    Force_plage = data.Force[plage]
    model = sm.OLS(Force_plage, Déplacement_matrice)
    results = model.fit()
    coef_corr = results.rsquared

#     # Affinage automatique de la plage d'ajustement :
# 
#     coef_corr = 0
#     while coef_corr < coef_lim:
#         Déplacement_plage = data.Déplacement[plage]
#         # Ajout d'une colonne constante pour obtenir un intercept
#         Déplacement_matrice = sm.add_constant(Déplacement_plage)
#         Force_plage = data.Force[plage]
#         model = sm.OLS(Force_plage, Déplacement_matrice)
#         results = model.fit()
#         coef_corr = results.rsquared
#         # Réduction de la plage par la fin
#         plage = slice(début_plage, plage.stop-1)
# 
#     if (plage.stop - plage.start) <= 3:
#         QMessageBox.warning(
#             None, f"Linéarisation impossible !",
#             """Impossible de trouver un domaine élastique pour la linéarisation.
# Réessayez avec de nouvelles bornes.""")
#         raise ValueError("Impossible de trouver un domaine élastique !")

    intercept, pente = results.params[0], results.params[1]
    Force_linéarisée = pente * Déplacement_plage + intercept

    # Tracé de la linéarisation de la force :
    figLinéarisationForce, axLinéarisationForce = plt.subplots()
    axLinéarisationForce.plot(Déplacement_plage, Force_plage, label="Force mesurée")
    axLinéarisationForce.plot(Déplacement_plage, Force_linéarisée,
                              label="Force linéarisée")
    axLinéarisationForce.set_title("Linéarisation de la force")
    axLinéarisationForce.set_xlabel("Déplacement (mm)")
    axLinéarisationForce.set_ylabel("Force (N)")
    axLinéarisationForce.legend()
    axLinéarisationForce.grid(True)
    figLinéarisationForce.savefig(nom_échantillon + "_courbe_force_linéarisée.pdf",
                                  bbox_inches='tight')

    # On obtient la rigidité de flexion EI à partir de la linéarisation, sachant que
    # d = F * L**3 / (48 * EI)  ou  F = (48 * EI / L**3) * d
    EI = (pente * L**3 / 48 )  # in N.mm²
    EI *= 10**-6  # in N.m²
    if EI < 0:
        QMessageBox.warning(None, f"Rigidité de flexion négative : {E}GPa",
                            """Le problème peut venir du choix des bornes, d'un bruit trop important ou de la personne qui a écrit le code.""")
        EI = "erreur!!"
        E = "erreur!!"
    else:
        d_max = data.Déplacement[plage.stop]
        E = (EI / I) * 10**-9  # in GPa
#         QMessageBox.information(
#             None, "Linéarisation réussie !",
#             f"""Après détection automatique, le déplacement maximal retenu pour la régression linéaire est de {d_max:.2e}mm.\n
#     Cela correspond à un coefficient de corrélation de {coef_corr:.6f}.\n
#     Alors, heureu·x·se ?""")
        QMessageBox.information(
            None, "Grandeurs mécaniques estimées",
            f"""Rigidité de flexion : EI = {EI:.2e} N.m²\n
Module d'élasticité : E = {E:.2e} GPa\n
Qualité de la linéarisation : R² = {coef_corr:.3e}""")

    # Export au format Excel :

    colonnes_à_exporter = ["Déplacement", "Force"]

    nom_colonnes = ["Déplacement de flexion (mm)", "Force de flexion (N)"]

    data.to_excel(nom_échantillon + ".xlsx", columns=colonnes_à_exporter, header=nom_colonnes,
                  index=False)

    # Résumé de l'analyse
    résumé = f"""Test de flexion 3 points avec la Deben sur l'échantillon '{nom_échantillon}'.

Longueur de la travée : {L} mm

Caractéristiques de l'échantillon :
-----------------------------------
Forme : {forme}
"""
    if forme == "Circulaire":
        résumé += f"""Diamètre : {e} mm
"""
    elif forme == "Ovale":
        résumé += f"""Diamètre (perpendiculaire) : {lg} mm
Diamètre (parallèle) : {e} mm
"""
    elif forme == "Rectangulaire":
        résumé += f"""Largeur : {lg} mm
Épaisseur : {e} mm
"""
    elif forme == "Triangulaire":
        résumé += f"""Largeur : {lg} mm
Épaisseur : {e} mm
"""
    résumé += f"""Moment quadratique : {I:.2e} m⁴

Linéarisation par régression linéaire :
---------------------------------------
Déplacement min pour la linéarisation : {data.Déplacement[plage.start]:.2e} mm
Déplacement max pour la linéarisation : {data.Déplacement[plage.stop]:.2e} mm
Nombre de points pour la linéarisation : {plage.stop + 1 - plage.start}
R² : {coef_corr:.6f}

Grandeurs mécaniques estimées :
-------------------------------
Rigidité de flexion : {EI:.2e} N.m²
Module élastique : {E:.2e} GPa"""

    # Écriture du résumé dans un document texte
    with open(nom_échantillon + ".txt", 'w') as f:
        f.write(résumé)

    # Fermeture de toutes les fenêtres Matplotlib
    plt.close('all')

    # Faut-il enchainer avec un autre échantillon ?
    réponse = QMessageBox.question(
            None,
            "Fin du traitement",
            "Voulez-vous traiter un autre échantillon ?",
            defaultButton=QMessageBox.Yes)
    if réponse == QMessageBox.Yes:
        continuer = True
    else:
        continuer = False
