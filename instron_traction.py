#!/usr/bin/env python
# coding: utf-8
# Félix Hartmann (felix.hartmann@inrae.fr)
# 1ère version : avril 2019
# dernière version : juillet 2021

"""
Ce script sert à caractériser le comportement mécanique global (MOE, MOR et
charge maximale) d'échantillons à partir de données acquises lors d'essais de
traction avec l'INSTRON 5565.
"""

import os
import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
from matplotlib.widgets import SpanSelector
from PyQt5.QtWidgets import QApplication, QFileDialog, QInputDialog, QMessageBox

app = QApplication([''])  # nécessaire pour utiliser les widgets PyQt

# Fichier rassemblant les résultats de tous les échantillons de la série
fichier_série, ok = QFileDialog.getSaveFileName(
        parent=None,
        caption="Fichier rassemblant les résultats de la série (optionnel)",
        filter="*.csv")

if ok and fichier_série:
    export_série = True
    if not fichier_série.endswith('.csv'):
        fichier_série += '.csv'
    dict_série = {'sample': [],
                  'L0': [],
                  'width': [],
                  'thickness': [],
                  'area': [],
                  'E': [],
                  'MOR': []}
else:
    export_série = False

# Coefficient de corrélation à utiliser pour la linéarisation :
coef_lim = 0.995
nouveau_coef_lim, ok = QInputDialog.getDouble(
    None, "Coefficient de corrélation", 
    f"""Le coefficient de corrélation minimal pour la linéarisation est de {coef_lim}.\n
Vous pouvez le modifier manuellement :""",
    value=coef_lim, decimals=4)
if ok:
    coef_lim = nouveau_coef_lim

# Longueur initiale de l'éprouvette (espacement entre les deux mords)
# Cette valeur est conservée en mémoire d'un échantillon à l'autre,
# car elle varie peu ou pas.
L0 = 0

continuer = True

while continuer:

    # Choix du fichier de données et du nom de l'échantillon :

    chemin_fichier, _ = QFileDialog.getOpenFileName(
            parent=None, caption="Sélection du fichier issu de l'INSTRON 5565",
            filter="*.csv")

    if not chemin_fichier:
        break

    dossier, nom_fichier = os.path.split(chemin_fichier)
    os.chdir(dossier)

    try:
        data = pd.read_csv(
                nom_fichier, sep=";", decimal=',', encoding='latin-1', header=None,
                skiprows=6, names=['Temps', 'Déplacement', 'Force'])
    except:
        err = sys.exc_info()[0]
        QMessageBox.warning(None, "Erreur lors de la lecture du fichier", err)
        continue

    nom_échantillon = QInputDialog.getText(None, "Nom de l'échantillon",
                                           "Nom de l'échantillon :")[0].replace(' ', '_')

    # Tracé de la courbe Force-Déplacement
    figForceDeplacement, axForceDeplacement = plt.subplots()
    data.plot("Déplacement", "Force", ax=axForceDeplacement, legend=False, grid=True)
    axForceDeplacement.set_title("Courbe Force-Déplacement")
    axForceDeplacement.set_xlabel("Déplacement (mm)")
    axForceDeplacement.set_ylabel("Force (N)")
    figForceDeplacement.savefig(nom_échantillon + "_courbe_force-déplacement.pdf",
                                bbox_inches='tight')
    plt.close(figForceDeplacement)

    # Longueur initiale de l'éprouvette (espacement entre les deux mords)
    ok = False
    while not ok or L0 <= 0:
        L0, ok = QInputDialog.getDouble(
                None,
                "Longueur initiale",
                "Longueur initiale (i.e. espacement initial entre les deux mords) en mm :",
                value=L0,
                decimals=0)
        if L0 <= 0:
            QMessageBox.warning(
                None, "Valeur incorrecte",
                """La longueur initiale doit être strictemet positive.""")

    # Largeur de la section
    l = 0
    while l <= 0:
        l, _ = QInputDialog.getDouble(
                None,
                "Largeur",
                "Largeur de la section transverse en mm :",
                decimals=3)
        if l <= 0:
            QMessageBox.warning(
                None, "Valeur incorrecte",
                """La largeur de la section transverse doit être strictemet positive.""")

    # Épaisseur de la section
    e = 0
    while e <= 0:
        e, _ = QInputDialog.getDouble(
                None,
                "Épaisseur",
                "Épaisseur de la section transverse en mm :",
                decimals=3)
        if e <= 0:
            QMessageBox.warning(
                None, "Valeur incorrecte",
                """L'épaisseur de la section transverse doit être strictemet positive.""")

    S = l * e  # mm²

    # Passage en contraintes et déformations

    # On transforme les déplacements en déformations
    data["Déformation"] = data.Déplacement / L0

    # On transforme les forces en contraintes (MPa)
    data["Contrainte"] = data.Force / S  # (N.mm¯² = MPa)

    # Choix semi-manuel des limites du domaine élastique :

    bornes = {"inf": data.Déformation.min(), "sup": data.Déformation.max()}

    def onselect(déf_min, déf_max):  # appelée lorsque la plage a été choisie
        bornes["inf"], bornes["sup"] = déf_min, déf_max
        réponse = QMessageBox.question(None, "Choix des bornes du domaines élastique",
                                       f"""Vous avez choisi les bornes suivantes :\n
    Borne inférieure : {déf_min:.2e}
    Borne supérieure : {déf_max:.2e}\n
La borne supérieure sera affinée automatiquement si besoin pour que le coefficient de corrélation minimal soit atteint.\n
Ce choix vous convient-il ?""", defaultButton=QMessageBox.Yes)
        if réponse == QMessageBox.Yes:
            plt.close(figContrainteDéformation)

    # Tracé de la courbe Contrainte-Déformation pour le choix manuel du domaine élastique
    figContrainteDéformation, axContrainteDéformation = plt.subplots()
    data.plot("Déformation", "Contrainte", ax=axContrainteDéformation, legend=False,
              grid=True)
    axContrainteDéformation.set_title("Veuillez sélectionner le domaine élastique")
    axContrainteDéformation.set_xlabel("Déformation")
    axContrainteDéformation.set_ylabel("Contrainte (MPa)")
    # Widget Matplotlib pour choisir graphiquement la plage :
    span = SpanSelector(axContrainteDéformation, onselect, 'horizontal', useblit=True,
                        rectprops=dict(alpha=0.5, facecolor='red'), span_stays=True)
    plt.show()

    # Initialisation de la plage d'ajustement :
    début_plage = (data.Déformation >= bornes["inf"]).idxmax()
    fin_plage = (data.Déformation >= bornes["sup"]).idxmax()
    plage = slice(début_plage, fin_plage)

    # Affinage automatique de la plage d'ajustement :

    coef_corr = 0
    while coef_corr < coef_lim:
        Déformation_plage = data.Déformation[plage]
        # Ajout d'une colonne constante pour obtenir un intercept
        Déformation_matrice = sm.add_constant(Déformation_plage)
        Contrainte_plage = data.Contrainte[plage]
        model = sm.OLS(Contrainte_plage, Déformation_matrice)
        results = model.fit()
        coef_corr = results.rsquared
                # Réduction de la plage par la fin
        plage = slice(début_plage, plage.stop-1)

    if (plage.stop - plage.start) <= 3:
        QMessageBox.warning(
            None, "Linéarisation impossible !",
            """Impossible de trouver un domaine élastique pour la linéarisation.
Réessayez avec de nouvelles bornes.""")
        continue

    intercept, pente = results.params[0], results.params[1]
    Contrainte_linéarisée = pente * Déformation_plage + intercept

    # Tracé de la linéarisation de la contrainte :
    figLinéarisationContrainte, axLinéarisationContrainte = plt.subplots()
    axLinéarisationContrainte.plot(Déformation_plage, Contrainte_plage,
                                   label="Contrainte mesurée")
    axLinéarisationContrainte.plot(Déformation_plage, Contrainte_linéarisée,
                                   label="Contrainte linéarisée")
    axLinéarisationContrainte.set_title("Linéarisation de la contrainte")
    axLinéarisationContrainte.set_xlabel("Déformation")
    axLinéarisationContrainte.set_ylabel("Contrainte (MPa)")
    axLinéarisationContrainte.legend()
    axLinéarisationContrainte.grid(True)
    figLinéarisationContrainte.savefig(nom_échantillon + "_courbe_contrainte_linéarisée.pdf",
                                       bbox_inches='tight')

    # On obtient le module d'élasticité à partir de la linéarisation :
    E = pente * 10**-3  # converion MPa → GPa
    if E < 0:
        QMessageBox.warning(None, f"Module d'élasticité négatif : {E}GPa",
                            """Le problème peut venir du choix des bornes, d'un bruit trop important ou de la personne qui a écrit le code.""")
        E = "erreur!!"
    else:
        déf_min = data.Déformation[plage.start]
        déf_max = data.Déformation[plage.stop]
        MOR = data.Contrainte.max()
        F_max = data.Force.max()
        QMessageBox.information(
            None, "Linéarisation réussie !",
            f"""Après détection automatique, les bornes retenues pour la régression linéaire sont les suivantes :\n
    Borne inférieure : {déf_min:.2e}
    Borne supérieure : {déf_max:.2e}
    Amplitude : {déf_max-déf_min:.2e}\n
Cela correspond à un coefficient de corrélation de {coef_corr:.6f}.\n
Alors, heureu·x·se ?""")
        QMessageBox.information(
            None, "Grandeurs mécaniques estimées",
            f"""Charge maximale : Fmax = {F_max:.2e} N\n
Module de rupture : MOR = {MOR:.2e} MPa\n
Module d'élasticité : E = {E:.2e} GPa""")

    # Tracé de la courbe Contrainte-Déformation :
    figContrainteDéformation, axContrainteDéformation = plt.subplots()
    data.plot("Déformation", "Contrainte", ax=axContrainteDéformation,
              legend=False, grid=True)
    axContrainteDéformation.set_title("Courbe Contrainte-Déformation")
    axContrainteDéformation.set_xlabel("Déformation")
    axContrainteDéformation.set_ylabel("Contrainte (MPa)")
    figContrainteDéformation.savefig(nom_échantillon + "_courbe_contrainte-déformation.pdf",
                                     bbox_inches='tight')

    # Export au format Excel :

    colonnes_à_exporter = [
            "Temps",
            "Déplacement",
            "Force",
            "Déformation",
            "Contrainte"]

    nom_colonnes = [
            "Temps (s)",
            "Déplacement (mm)",
            "Force (N)",
            "Déformation",
            "Contrainte (MPa)"]

    data.to_excel(nom_échantillon + ".xlsx",
                  columns=colonnes_à_exporter,
                  header=nom_colonnes,
                  index=False)

    # Résumé de l'analyse
    résumé = f"""Test de traction avec l'INSTRON 5565 sur l'échantillon '{nom_échantillon}'.

Caractéristiques de l'échantillon :
-----------------------------------
Longueur initiale : {L0} mm
Section transverse : {S:.2e} mm²

Linéarisation par régression linéaire :
---------------------------------------
Déformation min pour la linéarisation : {data.Déformation[plage.start]:.2e} MPa
Déformation max pour la linéarisation : {data.Déformation[plage.stop]:.2e} MPa
Nombre de points pour la linéarisation : {plage.stop + 1 - plage.start}
R² : {coef_corr:.6f}

Données mécaniques :
--------------------
Charge maximale : {F_max:.2e} N
Module de rupture : {MOR:.2e} MPa
Module élastique : {E:.2e} GPa"""

    # Écriture du résumé dans un document texte
    with open(nom_échantillon + ".txt", 'w') as f:
        f.write(résumé)

    if export_série:
        dict_série['sample'].append(nom_échantillon)
        dict_série['L0'].append(L0)
        dict_série['width'].append(l)
        dict_série['thickness'].append(e)
        dict_série['area'].append(S)
        dict_série['E'].append(E)
        dict_série['MOR'].append(MOR)
        # Écriture du fichier résumant la série (l'ancien est écrasé)
        df_série = pd.DataFrame(dict_série)
        df_série.to_csv(fichier_série, sep=';', index=False)

    # Fermeture de toutes les fenêtres Matplotlib
    plt.close('all')

    # Faut-il enchainer avec un autre échantillon ?
    réponse = QMessageBox.question(
            None,
            "Fin du traitement",
            "Voulez-vous traiter un autre échantillon ?",
            defaultButton=QMessageBox.Yes)
    if réponse == QMessageBox.Yes:
        continuer = True
    else:
        continuer = False
