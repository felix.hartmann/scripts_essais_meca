#!/usr/bin/env python
# coding: utf-8
# Félix Hartmann (felix.hartmann@inrae.fr)
# 1ère version : avril 2019
# dernière version : juillet 2021

"""
Ce script sert à caractériser le comportement mécanique global (MOE,
limite d'élasticité et contrainte maximale) d'éprouvettes à partir de
données acquises lors d'essais de flexion 3 points avec l'INSTRON 5565.

Il est adapté d'un script Matlab écrit par Benjamin Niez.
"""

import os, sys
import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
from matplotlib.widgets import SpanSelector
from PyQt5.QtWidgets import QApplication, QFileDialog, QInputDialog, QMessageBox

app = QApplication([''])  # nécessaire pour utiliser les widgets PyQt

# Fichier rassemblant les résultats de tous les échantillons de la série
fichier_série, ok = QFileDialog.getSaveFileName(
        parent=None,
        caption="Fichier rassemblant les résultats de la série (optionnel)",
        filter="*.csv")

if ok and fichier_série:
    export_série = True
    if not fichier_série.endswith('.csv'):
        fichier_série += '.csv'
    # dictionnaire pour l'export de la série dans un unique fichier csv
    dict_série = {'echantillon': [],
                  'forme': [],
                  'largeur (mm)': [],
                  'epaisseur (mm)': [],
                  'I (m4)': [],
                  'E (GPa)': [],
                  'EI (N.m2)': [],
                  'MOR (MPa)': [],
                  'energie de rupture (J)': []}
else:
    export_série = False

# Coefficient de corrélation à utiliser pour la linéarisation :
coef_lim = 0.995
nouveau_coef_lim, ok = QInputDialog.getDouble(
    None, "Coefficient de corrélation", 
    f"""Le coefficient de corrélation minimal pour la linéarisation est de {coef_lim}.\n
Vous pouvez le modifier manuellement :""",
    value=coef_lim, decimals=4)
if ok:
    coef_lim = nouveau_coef_lim

# Faut-il extrapoler la linéarisation à la phase initiale non-linéaire ?
réponse = QMessageBox.question(
        None,
        "Extrapolation linéaire à la phase initiale",
        """Voulez-vous extrapoler la linéarisation à la phase initiale non-linéaire de la courbe force-déplacement et utiliser l'intercept comme référence zéro pour la courbe de déformation ?""",
        defaultButton=QMessageBox.Yes)
if réponse == QMessageBox.Yes:
    extrapolation = True
else:
    extrapolation = False

formes = ["Rectangulaire", "Circulaire"]
forme_par_défaut = 0  # rectangulaire

# Longueur de la travée (espacement entre les deux appuis)
# Cette valeur est conservée en mémoire d'un échantillon à l'autre,
# car elle varie peu ou pas.
L = 0

continuer = True    # on parcourt les fichiers d'une série d'expériences

while continuer:

    # Choix du fichier de données et du nom de l'échantillon :
    chemin_fichier, _ = QFileDialog.getOpenFileName(
            parent=None, caption="Sélection du fichier issu de l'INSTRON 5565",
            filter="*.csv")

    if not chemin_fichier:
        break

    dossier, nom_fichier = os.path.split(chemin_fichier)
    os.chdir(dossier)

    try:
        data = pd.read_csv(
                nom_fichier, sep=";", decimal=',', encoding='latin-1', header=None,
                skiprows=2, names=['Temps', 'Déplacement', 'Force', 'NbCycles'])
    except:
        err = sys.exc_info()[0]
        QMessageBox.warning(None, "Erreur lors de la lecture du fichier", err)
        continue

    nom_échantillon = QInputDialog.getText(None, "Nom de l'échantillon",
                                           "Nom de l'échantillon :")[0].replace(' ', '_')

    ok = False
    while not ok or L <= 0:
        L, ok = QInputDialog.getDouble(
                None,
                "Longueur de la travée",
                "Longueur de la travée (i.e. espacement entre les deux appuis inférieurs) en mm :",
                value=L, decimals=0)
        if L <= 0:
            QMessageBox.warning(
                    None, "Valeur incorrecte",
                    """La longueur initiale doit être strictemet positive.""")

    # Géométrie de l'échantillon :

    forme, _ = QInputDialog.getItem(None, "Forme de l'échantillon",
            "Forme de l'échantillon :", formes, forme_par_défaut, False)
    forme_par_défaut = formes.index(forme)

    if forme == "Circulaire":
        lg, _ = QInputDialog.getDouble(
            None, "Diamètre ⊥ de l'échantillon",
            "Diamètre de l'échantillon dans la direction perpendiculaire à la flexion (mm) :",
            decimals=2)
        e, _ = QInputDialog.getDouble(
            None, "Diamètre // de l'échantillon",
            "Diamètre de l'échantillon dans la direction parallèle à la flexion (mm) :",
            decimals=2)
        # On calcule le moment quadratique de l'échantillon :
        I = (np.pi * lg * e**3 * 10**-12) / 64
    elif forme == "Rectangulaire":
        # L'épaisseur de l'échantillon correspond à sa dimension dans la direction de la flexion.
        e, _ = QInputDialog.getDouble(
            None, "Épaisseur de l'échantillon",
            "Épaisseur de l'échantillon dans la direction de la flexion (mm) :",
            decimals=2)
        lg, _ = QInputDialog.getDouble(None, "Largeur de l'échantillon",
                                       "Largeur de l'échantillon (mm) :", decimals=2)
        # On calcule le moment quadratique de l'échantillon :
        I = (lg * (e**3) * (10**(-12))) / 12

    # Il est possible d'entrer la valeur exacte du moment quadratique.
    I_exact, ok = QInputDialog.getDouble(
        None, "Moment quadratique de l'échantillon",
        f"""Le moment quadratique calculé est de {I:.2e}m⁴.\n
Si vous avez la valeur exacte du moment quadratique de la section à mi-travée, entrez-la manuellement :""",
        value=I, decimals=15)
    if ok:
        I = I_exact

    # Choix semi-manuel des limites du domaine élastique :

    bornes = {"inf": data.Déplacement.min(), "sup": data.Déplacement.max()}

    def onselect(dép_min, dép_max):  # appelée lorsque la plage a été choisie
        bornes["inf"], bornes["sup"] = dép_min, dép_max
        réponse = QMessageBox.question(None, "Choix des bornes du domaines élastique",
                                       f"""Vous avez choisi les bornes suivantes :\n
    Borne inférieure : {dép_min:.2e}mm
    Borne supérieure : {dép_max:.2e}mm\n
La borne supérieure sera affinée automatiquement si besoin pour que le coefficient de corrélation minimal soit atteint.\n
Ce choix vous convient-il ?""", defaultButton=QMessageBox.Yes)
        if réponse == QMessageBox.Yes:
            plt.close(figForceDéplacement)

    # Tracé de la courbe Force-Déplacement
    figForceDéplacement, axForceDéplacement = plt.subplots()
    data.plot("Déplacement", "Force", ax=axForceDéplacement, legend=False, grid=True)
    axForceDéplacement.set_title("Courbe Force-Déplacement")
    axForceDéplacement.set_xlabel("Déplacement (mm)")
    axForceDéplacement.set_ylabel("Force (N)")
    figForceDéplacement.savefig(nom_échantillon + "_courbe_force-déplacement.pdf",
                                bbox_inches='tight')

    axForceDéplacement.set_title("Veuillez sélectionner le domaine élastique")

    # Widget Matplotlib pour choisir graphiquement la plage :
    span = SpanSelector(axForceDéplacement, onselect, 'horizontal', useblit=True,
                        rectprops=dict(alpha=0.5, facecolor='red'), span_stays=True)
    plt.show()

    # Initialisation de la plage d'ajustement :
    début_plage = (data.Déplacement >= bornes["inf"]).idxmax()
    fin_plage = (data.Déplacement >= bornes["sup"]).idxmax()
    plage = slice(début_plage, fin_plage)

    # Affinage automatique de la plage d'ajustement :

    coef_corr = 0
    while coef_corr < coef_lim:
        Déplacement_plage = data.Déplacement[plage]
        # Ajout d'une colonne constante pour obtenir un intercept
        Déplacement_matrice = sm.add_constant(Déplacement_plage)
        Force_plage = data.Force[plage]
        model = sm.OLS(Force_plage, Déplacement_matrice)
        results = model.fit()
        coef_corr = results.rsquared
        # Réduction de la plage par la fin
        plage = slice(début_plage, plage.stop-1)

    if (plage.stop - plage.start) <= 3:
        QMessageBox.warning(
            None, f"Linéarisation impossible !",
            """Impossible de trouver un domaine élastique pour la linéarisation.
Réessayez avec de nouvelles bornes.""")
        continue

    intercept, pente = results.params[0], results.params[1]

    Déplacement_linéarisé = np.linspace(0, data.Déplacement.max(), 100)
    Force_linéarisée = pente * Déplacement_linéarisé + intercept 
    Force_linéarisée[Force_linéarisée < 0] = 0

    if extrapolation:
        d0 = -intercept / pente  # intersection Force linéarisée avec axe des x
        idx0 = (data.Déplacement >= d0).idxmax()  # indice proche de d0
        data["Force_extrapolée"] = data.Force[:]
        data.Force_extrapolée[:idx0] = 0
        data.Force_extrapolée[idx0:plage.start] = \
                pente * (data.Déplacement[idx0:plage.start] - data.Déplacement[idx0])

    # Calcul de l'énergie de rupture

    # Force
    if extrapolation:
        Fmax_idx = data.Force_extrapolée.argmax()  # index de la force de rupture
        F = np.array(data.Force_extrapolée[:Fmax_idx])
    else:
        Fmax_idx = data.Force.argmax()  # index de la force de rupture
        F = np.array(data.Force[:Fmax_idx])

    # Déplacements entre points
    delta_d = np.array(data.Déplacement[1:Fmax_idx+1]) - np.array(data.Déplacement[:Fmax_idx])
    delta_d /= 1000  # conversion mm → m

    E_rupture = np.sum(F * delta_d)

    # Passage en contraintes et déformations

    # On transforme les déplacements en déformations (cas de la flexion 3 points)
    # avec formule établie à partir de celle de la flèche et de celle de sigma.
    data["Déformation"] = 6 * e * data.Déplacement / L**2

    Déformation_linéarisée = 6 * e * Déplacement_linéarisé / L**2

    # On transforme les forces en contraintes maximales (en e/2).
    # Moment fléchissant à mi-travée :
    if extrapolation:
        data["Moment"] = (data.Force_extrapolée * L * 10**-3) / 4
    else:
        data["Moment"] = (data.Force * L * 10**-3) / 4
    
    Moment_linéarisé = (Force_linéarisée * L * 10**-3) / 4

    # Contrainte à mi-travée (MPa) :
    data["Contrainte"] = (data.Moment * (e / 2) / I) * 10**-9  # MPa

    Contrainte_linéarisée = (Moment_linéarisé * (e / 2) / I) * 10**-9  # MPa

    # Tracé de la linéarisation de la contrainte :
    figLinéarisationContrainte, axLinéarisationContrainte = plt.subplots()
    axLinéarisationContrainte.plot(data.Déformation, data.Contrainte,
                                   label="Contrainte mesurée")
    axLinéarisationContrainte.plot(Déformation_linéarisée, Contrainte_linéarisée,
                                   label="Contrainte linéarisée")
    axLinéarisationContrainte.set_title("Linéarisation de la contrainte")
    axLinéarisationContrainte.set_xlabel("Déformation")
    axLinéarisationContrainte.set_ylabel("Contrainte (MPa)")
    axLinéarisationContrainte.legend()
    axLinéarisationContrainte.grid(True)
    figLinéarisationContrainte.savefig(nom_échantillon + "_courbe_contrainte_linéarisée.pdf",
                                       bbox_inches='tight')

    # On obtient le module d'élasticité à partir de la linéarisation de la courbe
    # Force-Déplacement, et en faisant les transformations nécessaires, sachant que :
    # pente en N/mm
    # L en mm
    # I en m^4
    E = (pente * 10**3) * ((L * 10**-3)**3 / (48 * I)) * 10**-9  # en GPa
    if E < 0:
        QMessageBox.warning(None, f"Module d'élasticité négatif : {E}GPa",
                            """Le problème peut venir du choix des bornes, d'un bruit trop important ou de la personne qui a écrit le code.""")
        E = "erreur!!"
    else:
        EI = (E * 10**9) * I  # en N.m²
        MOR = data.Contrainte.max()
        dep_max = data.Déplacement[plage.stop]
        QMessageBox.information(
            None, "Linéarisation réussie !",
            f"""Après détection automatique, le déplacement maximal retenu pour la régression linéaire est de {dep_max:.2e} mm.\n
Cela correspond à un coefficient de corrélation de {coef_corr:.6f}.\n
Alors, heureu·x·se ?""")
        QMessageBox.information(
            None, "Grandeurs mécaniques estimées",
            f"""Module d'élasticité : E = {E:.2e} GPa\n
Rigidité de flexion : EI = {EI:.2e} N.m²\n
Module de rupture : MOR = {MOR:.2e} MPa\n
Énergie de rupture : E_rupt = {E_rupture:.2e} J\n
Attention ! L'estimation du module de rupture et de l'énergie de rupture ne sont pertinentes que si la rupture a été atteinte lors du test mécanique.""")

    # Tracé de la courbe Contrainte-Déformation :
    figContrainteDéformation, axContrainteDéformation = plt.subplots()
    data.plot("Déformation", "Contrainte", ax=axContrainteDéformation,
              legend=False, grid=True)
    axContrainteDéformation.set_title("Courbe Contrainte-Déformation")
    axContrainteDéformation.set_xlabel("Déformation")
    axContrainteDéformation.set_ylabel("Contrainte (MPa)")
    figContrainteDéformation.savefig(
            nom_échantillon + "_courbe_contrainte-déformation.pdf", bbox_inches='tight')

    # Export au format Excel :

    colonnes_à_exporter = [
        "Temps",
        "Déplacement",
        "Force",
        "Déformation",
        "Contrainte",
        "Moment"]

    nom_colonnes = [
        "Temps (s)",
        "Déplacement de flexion (mm)",
        "Force de flexion (N)",
        "Déformation à mi-travée",
        "Contrainte à mi-travée (MPa)",
        "Moment fléchissant (N.m²)"]

    data.to_excel(nom_échantillon + ".xlsx", columns=colonnes_à_exporter, header=nom_colonnes,
                  index=False)

    # Résumé de l'analyse
    résumé = f"""Test de flexion 3 points avec l'INSTRON 5565 sur l'échantillon '{nom_échantillon}'.

Caractéristiques de l'échantillon :
-----------------------------------
"""
    if forme == "Circulaire":
        résumé += f"""Diamètre (perpendiculaire) : {lg} mm
Diamètre (parallèle) : {e} mm
Longueur de la travée : {L} mm
"""
    elif forme == "Rectangulaire":
        résumé += f"""Largeur : {lg} mm
Épaisseur : {e} mm
Longueur de la travée : {L} mm
"""
    résumé += f"""Moment quadratique : {I:.2e} m4

Linéarisation par régression linéaire :
---------------------------------------
Déformation min pour la linéarisation : {data.Déformation[plage.start]:.2e} MPa
Déformation max pour la linéarisation : {data.Déformation[plage.stop]:.2e} MPa
Nombre de points pour la linéarisation : {plage.stop + 1 - plage.start}
R² : {coef_corr:.6f}

Données mécaniques :
--------------------
Module élastique : {E:.2e} GPa
Rigidité de flexion : {EI:.2e} N.m2
Résistance à la rupture : {MOR:.2e} MPa
Énergie de rupture : {E_rupture:.2e} J"""

    # Écriture du résumé dans un document texte
    with open(nom_échantillon + ".txt", 'w') as f:
        f.write(résumé)

    if export_série:
        dict_série['echantillon'].append(nom_échantillon)
        dict_série['forme'].append(forme)
        dict_série['largeur (mm)'].append(lg),
        dict_série['epaisseur (mm)'].append(e),
        dict_série['I (m4)'].append(I)
        dict_série['E (GPa)'].append(E)
        dict_série['EI (N.m2)'].append(EI)
        dict_série['MOR (MPa)'].append(MOR)
        dict_série['energie de rupture (J)'].append(E_rupture)

    # Fermeture de toutes les fenêtres Matplotlib
    plt.close('all')

    if export_série:
        df_série = pd.DataFrame(dict_série)
        df_série.to_csv(fichier_série, sep=';', index=False)

    # Faut-il enchainer avec un autre échantillon ?
    réponse = QMessageBox.question(
            None,
            "Fin du traitement",
            "Voulez-vous traiter un autre échantillon ?",
            defaultButton=QMessageBox.Yes)
    if réponse == QMessageBox.Yes:
        continuer = True
    else:
        continuer = False
