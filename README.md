# Scripts pour le dépouillement semi-automatique d'essais mécaniques

Ces scripts sont écrits spécifiquement pour les machines d'essais mécaniques de l'[UMR PIAF](https://www6.clermont.inrae.fr/piaf/) :

1. Instron 5565 équipée d'une cellule de force de 1kN, utilisée pour les essais en flexion trois points et en traction.
2. Deben équipée d'une cellule de force de 50N, utilisée pour les micro-essais en flexion trois points.

Liste des scripts :

* `instron_flexion_1_cycle.py` : essais en flexion trois points avec l'Instron, avec un seul cycle charge/décharge.
* `instron_flexion_3_cycles.py` : essais en flexion trois points avec l'Instron, avec trois cycles charge/décharge et estimation des paramètres uniquement sur le troisième cycle.
* `instron_traction.py` : essais en traction avec l'Instron.
* `deben_fr.py` : essais en flexion trois points avec la Deben (version avec interface en français).
* `deben_en.py` : essais en flexion trois points avec la Deben (version avec interface en anglais).

# Installation

Les scripts sont écrits en Python et dépendent des bibliothèques suivantes :

* Numpy
* Pandas
* Matplotlib
* Statsmodels
* PyQt5

Ces bibliothèques sont toutes incluses dans la distribution Python Anaconda :
https://www.anaconda.com/products/individual

# Exécution

## Windows

Chaque script `.py` a un lanceur automatique `.bat` du même nom. Double-cliquez sur le `.bat` pour lancer le script.

## Linux et Mac OS X

Ouvrez un terminal dans le bon répertoire et lancez `python ./instron_traction.py` (ou un des autres scripts).

# Utilisation

Laissez-vous guider par l'interface utilisateur.
